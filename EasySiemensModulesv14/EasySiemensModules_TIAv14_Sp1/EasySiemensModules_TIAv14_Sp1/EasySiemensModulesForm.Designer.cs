﻿namespace EasySiemensModules_TIAv14_Sp1
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grp_TIA = new System.Windows.Forms.GroupBox();
            this.lbl_Step1 = new System.Windows.Forms.Label();
            this.rdb_WithoutUI = new System.Windows.Forms.RadioButton();
            this.rdb_WithUI = new System.Windows.Forms.RadioButton();
            this.btn_DisposeTIA = new System.Windows.Forms.Button();
            this.btn_StartTIA = new System.Windows.Forms.Button();
            this.txt_Status = new System.Windows.Forms.TextBox();
            this.grp_GetProjects = new System.Windows.Forms.GroupBox();
            this.btn_UpdateProjects = new System.Windows.Forms.Button();
            this.lbl_Step2 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lbl_Projects = new System.Windows.Forms.Label();
            this.frm_ProjectList = new System.Windows.Forms.ListView();
            this.Project_Name = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ProjectPath = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Status = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.NoModules = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ReadTimeStamp = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.grp_ReadProjects = new System.Windows.Forms.GroupBox();
            this.frm_Status = new System.Windows.Forms.Label();
            this.btn_ReadSelectedProjets = new System.Windows.Forms.Button();
            this.btn_ReadAllProjets = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.grp_tools = new System.Windows.Forms.GroupBox();
            this.btn_CompareWithStandardized = new System.Windows.Forms.Button();
            this.btn_ExportToExcel = new System.Windows.Forms.Button();
            this.FRM_Wait = new System.Windows.Forms.PictureBox();
            this.chk_NeedAutoUpdate = new System.Windows.Forms.CheckBox();
            this.btn_ExportToText = new System.Windows.Forms.Button();
            this.btn_ExportToTextOnlyOrderNumbers = new System.Windows.Forms.Button();
            this.grp_TIA.SuspendLayout();
            this.grp_GetProjects.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.grp_ReadProjects.SuspendLayout();
            this.grp_tools.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FRM_Wait)).BeginInit();
            this.SuspendLayout();
            // 
            // grp_TIA
            // 
            this.grp_TIA.Controls.Add(this.lbl_Step1);
            this.grp_TIA.Controls.Add(this.rdb_WithoutUI);
            this.grp_TIA.Controls.Add(this.rdb_WithUI);
            this.grp_TIA.Controls.Add(this.btn_DisposeTIA);
            this.grp_TIA.Controls.Add(this.btn_StartTIA);
            this.grp_TIA.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.grp_TIA.Location = new System.Drawing.Point(15, 36);
            this.grp_TIA.Name = "grp_TIA";
            this.grp_TIA.Size = new System.Drawing.Size(185, 238);
            this.grp_TIA.TabIndex = 17;
            this.grp_TIA.TabStop = false;
            this.grp_TIA.Text = "TIA Portal Connection";
            this.grp_TIA.Enter += new System.EventHandler(this.grp_TIA_Enter);
            // 
            // lbl_Step1
            // 
            this.lbl_Step1.AutoSize = true;
            this.lbl_Step1.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Step1.Font = new System.Drawing.Font("Arial", 20F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl_Step1.ForeColor = System.Drawing.Color.Navy;
            this.lbl_Step1.Location = new System.Drawing.Point(30, 26);
            this.lbl_Step1.Name = "lbl_Step1";
            this.lbl_Step1.Size = new System.Drawing.Size(107, 32);
            this.lbl_Step1.TabIndex = 19;
            this.lbl_Step1.Text = "STEP 1";
            // 
            // rdb_WithoutUI
            // 
            this.rdb_WithoutUI.AutoSize = true;
            this.rdb_WithoutUI.Checked = true;
            this.rdb_WithoutUI.Location = new System.Drawing.Point(36, 95);
            this.rdb_WithoutUI.Name = "rdb_WithoutUI";
            this.rdb_WithoutUI.Size = new System.Drawing.Size(145, 19);
            this.rdb_WithoutUI.TabIndex = 2;
            this.rdb_WithoutUI.TabStop = true;
            this.rdb_WithoutUI.Text = "Without User Interface";
            this.rdb_WithoutUI.UseVisualStyleBackColor = true;
            // 
            // rdb_WithUI
            // 
            this.rdb_WithUI.AutoSize = true;
            this.rdb_WithUI.Location = new System.Drawing.Point(36, 72);
            this.rdb_WithUI.Name = "rdb_WithUI";
            this.rdb_WithUI.Size = new System.Drawing.Size(128, 19);
            this.rdb_WithUI.TabIndex = 1;
            this.rdb_WithUI.Text = "With User Interface";
            this.rdb_WithUI.UseVisualStyleBackColor = true;
            // 
            // btn_DisposeTIA
            // 
            this.btn_DisposeTIA.Enabled = false;
            this.btn_DisposeTIA.Location = new System.Drawing.Point(36, 184);
            this.btn_DisposeTIA.Name = "btn_DisposeTIA";
            this.btn_DisposeTIA.Size = new System.Drawing.Size(115, 36);
            this.btn_DisposeTIA.TabIndex = 4;
            this.btn_DisposeTIA.Text = "Dispose TIA";
            this.btn_DisposeTIA.UseVisualStyleBackColor = true;
            this.btn_DisposeTIA.Click += new System.EventHandler(this.btn_DisposeTIA_Click);
            // 
            // btn_StartTIA
            // 
            this.btn_StartTIA.Location = new System.Drawing.Point(36, 129);
            this.btn_StartTIA.Name = "btn_StartTIA";
            this.btn_StartTIA.Size = new System.Drawing.Size(115, 36);
            this.btn_StartTIA.TabIndex = 0;
            this.btn_StartTIA.Text = "Start TIA";
            this.btn_StartTIA.UseVisualStyleBackColor = true;
            this.btn_StartTIA.Click += new System.EventHandler(this.btn_StartTIA_Click);
            // 
            // txt_Status
            // 
            this.txt_Status.BackColor = System.Drawing.SystemColors.Menu;
            this.txt_Status.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txt_Status.Location = new System.Drawing.Point(0, 533);
            this.txt_Status.Multiline = true;
            this.txt_Status.Name = "txt_Status";
            this.txt_Status.Size = new System.Drawing.Size(1180, 45);
            this.txt_Status.TabIndex = 18;
            this.txt_Status.TextChanged += new System.EventHandler(this.txt_Status_TextChanged);
            // 
            // grp_GetProjects
            // 
            this.grp_GetProjects.Controls.Add(this.btn_UpdateProjects);
            this.grp_GetProjects.Controls.Add(this.lbl_Step2);
            this.grp_GetProjects.Enabled = false;
            this.grp_GetProjects.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.grp_GetProjects.Location = new System.Drawing.Point(206, 36);
            this.grp_GetProjects.Name = "grp_GetProjects";
            this.grp_GetProjects.Size = new System.Drawing.Size(185, 237);
            this.grp_GetProjects.TabIndex = 19;
            this.grp_GetProjects.TabStop = false;
            this.grp_GetProjects.Text = "Get Local TIA Projects";
            // 
            // btn_UpdateProjects
            // 
            this.btn_UpdateProjects.Location = new System.Drawing.Point(28, 129);
            this.btn_UpdateProjects.Name = "btn_UpdateProjects";
            this.btn_UpdateProjects.Size = new System.Drawing.Size(130, 36);
            this.btn_UpdateProjects.TabIndex = 20;
            this.btn_UpdateProjects.Text = "Update Project List";
            this.btn_UpdateProjects.UseVisualStyleBackColor = true;
            this.btn_UpdateProjects.Click += new System.EventHandler(this.btn_UpdateProjects_Click);
            // 
            // lbl_Step2
            // 
            this.lbl_Step2.AutoSize = true;
            this.lbl_Step2.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Step2.Font = new System.Drawing.Font("Arial", 20F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl_Step2.ForeColor = System.Drawing.Color.Navy;
            this.lbl_Step2.Location = new System.Drawing.Point(33, 26);
            this.lbl_Step2.Name = "lbl_Step2";
            this.lbl_Step2.Size = new System.Drawing.Size(107, 32);
            this.lbl_Step2.TabIndex = 20;
            this.lbl_Step2.Text = "STEP 2";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1180, 24);
            this.menuStrip1.TabIndex = 21;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.settingsToolStripMenuItem.Text = "Settings";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // lbl_Projects
            // 
            this.lbl_Projects.AutoSize = true;
            this.lbl_Projects.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Projects.Font = new System.Drawing.Font("Arial", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl_Projects.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lbl_Projects.Location = new System.Drawing.Point(393, 36);
            this.lbl_Projects.Name = "lbl_Projects";
            this.lbl_Projects.Size = new System.Drawing.Size(122, 23);
            this.lbl_Projects.TabIndex = 21;
            this.lbl_Projects.Text = "TIA Projects";
            // 
            // frm_ProjectList
            // 
            this.frm_ProjectList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.frm_ProjectList.AutoArrange = false;
            this.frm_ProjectList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Project_Name,
            this.ProjectPath,
            this.Status,
            this.NoModules,
            this.ReadTimeStamp});
            this.frm_ProjectList.FullRowSelect = true;
            this.frm_ProjectList.Location = new System.Drawing.Point(396, 62);
            this.frm_ProjectList.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.frm_ProjectList.Name = "frm_ProjectList";
            this.frm_ProjectList.Size = new System.Drawing.Size(773, 463);
            this.frm_ProjectList.TabIndex = 22;
            this.frm_ProjectList.UseCompatibleStateImageBehavior = false;
            this.frm_ProjectList.View = System.Windows.Forms.View.Details;
            // 
            // Project_Name
            // 
            this.Project_Name.Text = "Project Name";
            this.Project_Name.Width = 221;
            // 
            // ProjectPath
            // 
            this.ProjectPath.Text = "Path";
            this.ProjectPath.Width = 210;
            // 
            // Status
            // 
            this.Status.Text = "Status";
            // 
            // NoModules
            // 
            this.NoModules.Text = "Number of Modules";
            this.NoModules.Width = 106;
            // 
            // ReadTimeStamp
            // 
            this.ReadTimeStamp.Text = "Last read Date and Time";
            this.ReadTimeStamp.Width = 136;
            // 
            // grp_ReadProjects
            // 
            this.grp_ReadProjects.Controls.Add(this.chk_NeedAutoUpdate);
            this.grp_ReadProjects.Controls.Add(this.frm_Status);
            this.grp_ReadProjects.Controls.Add(this.btn_ReadSelectedProjets);
            this.grp_ReadProjects.Controls.Add(this.btn_ReadAllProjets);
            this.grp_ReadProjects.Controls.Add(this.label1);
            this.grp_ReadProjects.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.grp_ReadProjects.Location = new System.Drawing.Point(15, 288);
            this.grp_ReadProjects.Name = "grp_ReadProjects";
            this.grp_ReadProjects.Size = new System.Drawing.Size(185, 237);
            this.grp_ReadProjects.TabIndex = 21;
            this.grp_ReadProjects.TabStop = false;
            this.grp_ReadProjects.Text = "Read Device Configuration";
            // 
            // frm_Status
            // 
            this.frm_Status.BackColor = System.Drawing.Color.Transparent;
            this.frm_Status.Font = new System.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.frm_Status.ForeColor = System.Drawing.Color.Navy;
            this.frm_Status.Location = new System.Drawing.Point(6, 194);
            this.frm_Status.Name = "frm_Status";
            this.frm_Status.Size = new System.Drawing.Size(173, 32);
            this.frm_Status.TabIndex = 22;
            this.frm_Status.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_ReadSelectedProjets
            // 
            this.btn_ReadSelectedProjets.Location = new System.Drawing.Point(36, 118);
            this.btn_ReadSelectedProjets.Name = "btn_ReadSelectedProjets";
            this.btn_ReadSelectedProjets.Size = new System.Drawing.Size(115, 44);
            this.btn_ReadSelectedProjets.TabIndex = 21;
            this.btn_ReadSelectedProjets.Text = "Read Selected Projects";
            this.btn_ReadSelectedProjets.UseVisualStyleBackColor = true;
            // 
            // btn_ReadAllProjets
            // 
            this.btn_ReadAllProjets.Location = new System.Drawing.Point(36, 76);
            this.btn_ReadAllProjets.Name = "btn_ReadAllProjets";
            this.btn_ReadAllProjets.Size = new System.Drawing.Size(115, 36);
            this.btn_ReadAllProjets.TabIndex = 20;
            this.btn_ReadAllProjets.Text = "Read All Projects";
            this.btn_ReadAllProjets.UseVisualStyleBackColor = true;
            this.btn_ReadAllProjets.Click += new System.EventHandler(this.btn_ReadAllProjets_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Arial", 20F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(33, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 32);
            this.label1.TabIndex = 20;
            this.label1.Text = "STEP 3";
            // 
            // grp_tools
            // 
            this.grp_tools.Controls.Add(this.btn_ExportToTextOnlyOrderNumbers);
            this.grp_tools.Controls.Add(this.btn_ExportToText);
            this.grp_tools.Controls.Add(this.btn_CompareWithStandardized);
            this.grp_tools.Controls.Add(this.btn_ExportToExcel);
            this.grp_tools.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.grp_tools.Location = new System.Drawing.Point(206, 288);
            this.grp_tools.Name = "grp_tools";
            this.grp_tools.Size = new System.Drawing.Size(185, 237);
            this.grp_tools.TabIndex = 22;
            this.grp_tools.TabStop = false;
            this.grp_tools.Text = "Tools";
            // 
            // btn_CompareWithStandardized
            // 
            this.btn_CompareWithStandardized.Enabled = false;
            this.btn_CompareWithStandardized.Location = new System.Drawing.Point(28, 163);
            this.btn_CompareWithStandardized.Name = "btn_CompareWithStandardized";
            this.btn_CompareWithStandardized.Size = new System.Drawing.Size(130, 63);
            this.btn_CompareWithStandardized.TabIndex = 21;
            this.btn_CompareWithStandardized.Text = "Compare with Standardized Equipments";
            this.btn_CompareWithStandardized.UseVisualStyleBackColor = true;
            // 
            // btn_ExportToExcel
            // 
            this.btn_ExportToExcel.Enabled = false;
            this.btn_ExportToExcel.Location = new System.Drawing.Point(28, 28);
            this.btn_ExportToExcel.Name = "btn_ExportToExcel";
            this.btn_ExportToExcel.Size = new System.Drawing.Size(130, 36);
            this.btn_ExportToExcel.TabIndex = 20;
            this.btn_ExportToExcel.Text = "Export to Excel";
            this.btn_ExportToExcel.UseVisualStyleBackColor = true;
            // 
            // FRM_Wait
            // 
            this.FRM_Wait.Image = global::EasySiemensModules_TIAv14_Sp1.Properties.Resources.wait;
            this.FRM_Wait.Location = new System.Drawing.Point(689, 233);
            this.FRM_Wait.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.FRM_Wait.Name = "FRM_Wait";
            this.FRM_Wait.Size = new System.Drawing.Size(114, 113);
            this.FRM_Wait.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.FRM_Wait.TabIndex = 37;
            this.FRM_Wait.TabStop = false;
            this.FRM_Wait.Visible = false;
            // 
            // chk_NeedAutoUpdate
            // 
            this.chk_NeedAutoUpdate.AutoSize = true;
            this.chk_NeedAutoUpdate.Checked = true;
            this.chk_NeedAutoUpdate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_NeedAutoUpdate.Location = new System.Drawing.Point(16, 169);
            this.chk_NeedAutoUpdate.Name = "chk_NeedAutoUpdate";
            this.chk_NeedAutoUpdate.Size = new System.Drawing.Size(148, 19);
            this.chk_NeedAutoUpdate.TabIndex = 23;
            this.chk_NeedAutoUpdate.Text = "Open with AutoUpdate";
            this.chk_NeedAutoUpdate.UseVisualStyleBackColor = true;
            // 
            // btn_ExportToText
            // 
            this.btn_ExportToText.Location = new System.Drawing.Point(28, 70);
            this.btn_ExportToText.Name = "btn_ExportToText";
            this.btn_ExportToText.Size = new System.Drawing.Size(130, 36);
            this.btn_ExportToText.TabIndex = 22;
            this.btn_ExportToText.Text = "Export to Text";
            this.btn_ExportToText.UseVisualStyleBackColor = true;
            this.btn_ExportToText.Click += new System.EventHandler(this.btn_ExportToText_Click);
            // 
            // btn_ExportToTextOnlyOrderNumbers
            // 
            this.btn_ExportToTextOnlyOrderNumbers.Location = new System.Drawing.Point(28, 112);
            this.btn_ExportToTextOnlyOrderNumbers.Name = "btn_ExportToTextOnlyOrderNumbers";
            this.btn_ExportToTextOnlyOrderNumbers.Size = new System.Drawing.Size(130, 45);
            this.btn_ExportToTextOnlyOrderNumbers.TabIndex = 23;
            this.btn_ExportToTextOnlyOrderNumbers.Text = "Export only OrderNumbers";
            this.btn_ExportToTextOnlyOrderNumbers.UseVisualStyleBackColor = true;
            this.btn_ExportToTextOnlyOrderNumbers.Click += new System.EventHandler(this.btn_ExportToTextOnlyOrderNumbers_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1180, 578);
            this.Controls.Add(this.FRM_Wait);
            this.Controls.Add(this.grp_tools);
            this.Controls.Add(this.grp_ReadProjects);
            this.Controls.Add(this.frm_ProjectList);
            this.Controls.Add(this.lbl_Projects);
            this.Controls.Add(this.grp_GetProjects);
            this.Controls.Add(this.txt_Status);
            this.Controls.Add(this.grp_TIA);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(1196, 594);
            this.Name = "MainForm";
            this.Opacity = 0D;
            this.Text = "Easy Siemens Modules for TIA v14 Sp1";
            this.grp_TIA.ResumeLayout(false);
            this.grp_TIA.PerformLayout();
            this.grp_GetProjects.ResumeLayout(false);
            this.grp_GetProjects.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.grp_ReadProjects.ResumeLayout(false);
            this.grp_ReadProjects.PerformLayout();
            this.grp_tools.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FRM_Wait)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grp_TIA;
        private System.Windows.Forms.RadioButton rdb_WithoutUI;
        private System.Windows.Forms.RadioButton rdb_WithUI;
        private System.Windows.Forms.Button btn_DisposeTIA;
        private System.Windows.Forms.Button btn_StartTIA;
        private System.Windows.Forms.TextBox txt_Status;
        private System.Windows.Forms.Label lbl_Step1;
        private System.Windows.Forms.GroupBox grp_GetProjects;
        private System.Windows.Forms.Label lbl_Step2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Button btn_UpdateProjects;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.Label lbl_Projects;
        public System.Windows.Forms.ListView frm_ProjectList;
        private System.Windows.Forms.GroupBox grp_ReadProjects;
        private System.Windows.Forms.Button btn_ReadAllProjets;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_ReadSelectedProjets;
        private System.Windows.Forms.GroupBox grp_tools;
        private System.Windows.Forms.Button btn_ExportToExcel;
        private System.Windows.Forms.Button btn_CompareWithStandardized;
        private System.Windows.Forms.ColumnHeader Project_Name;
        private System.Windows.Forms.ColumnHeader ProjectPath;
        private System.Windows.Forms.ColumnHeader NoModules;
        private System.Windows.Forms.ColumnHeader ReadTimeStamp;
        private System.Windows.Forms.PictureBox FRM_Wait;
        private System.Windows.Forms.ColumnHeader Status;
        private System.Windows.Forms.Label frm_Status;
        private System.Windows.Forms.CheckBox chk_NeedAutoUpdate;
        private System.Windows.Forms.Button btn_ExportToText;
        private System.Windows.Forms.Button btn_ExportToTextOnlyOrderNumbers;
    }
}

