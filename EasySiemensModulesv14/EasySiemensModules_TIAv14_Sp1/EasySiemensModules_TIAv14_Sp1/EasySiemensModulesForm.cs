﻿using Microsoft.Win32;
using Siemens.Engineering;
using Siemens.Engineering.Compiler;
using Siemens.Engineering.Hmi;
using Siemens.Engineering.HW;
using Siemens.Engineering.HW.Features;
using Siemens.Engineering.SW;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace EasySiemensModules_TIAv14_Sp1
{
    public partial class MainForm : Form
    {
        public class TIADevice
        {
            public string TypeName { get; set; }
            public string TypeIdentifier { get; set; }
            public string Name { get; set; }
            public string IsGsd { get; set; }
            public List<TIADeviceItem> TIADeviceItems { get; set; }
            public TIADevice()
            {
                TypeName = string.Empty;
                TypeIdentifier = string.Empty;
                Name = string.Empty;
                IsGsd = string.Empty;
                TIADeviceItems = new List<TIADeviceItem>();
            }
        }
        public class TIADeviceItem
        {
            public string TypeName { get; set; }
            public string OrderNumber { get; set; }
            public string FirmwareVersion { get; set; }
            public string Name { get; set; }
            public string TypeIdentifier { get; set; }
            public string GsdName { get; set; }
            public TIADeviceItem()
            {
                TypeName = string.Empty;
                OrderNumber = string.Empty;
                FirmwareVersion = string.Empty;
                Name = string.Empty;
                TypeIdentifier = string.Empty;
                GsdName = string.Empty;
            }
        }
        public class TIAProject
        {
            public string Status { get; set; }
            public string Project_Name { get; set; }
            public string Path { get; set; }
            public string Num_CPUs { get; set; }
            public string Num_Modules { get; set; }
            public string TimeStamp { get; set; }
            public List<TIADevice> TIADevices { get; set; }
            public TIAProject()
            {
                Status = string.Empty;
                Project_Name = string.Empty;
                Path = string.Empty;
                Num_CPUs = string.Empty;
                Num_Modules = string.Empty;
                TimeStamp = string.Empty;
                TIADevices = new List<TIADevice>();
            }
        }

        TIADevice AktTIADevice = new TIADevice();
        TIADeviceItem AktTIADeviceItem = new TIADeviceItem();

        private static TiaPortalProcess _tiaProcess;

        int AktProjectNumber = 0;
        int SumModules = 0;
        int SumCPUs = 0;

        List<TIAProject> TIAProjects = new List<TIAProject>();

        public TiaPortal MyTiaPortal
        {
            get; set;
        }
        public Project MyProject
        {
            get; set;
        }

        SplashForm Splash = new SplashForm();

        TIAaccess Access = new TIAaccess();
        
        public MainForm()
        {
            InitializeComponent();
            AppDomain CurrentDomain = AppDomain.CurrentDomain;
            CurrentDomain.AssemblyResolve += new ResolveEventHandler(TIAResolver);
            Splash.ShowDialog();
            this.Opacity = 100;
        }

        private static Assembly TIAResolver(object sender, ResolveEventArgs args)
        {
            int index = args.Name.IndexOf(',');
            if (index == -1)
            {

                return null;
            }
            string name = args.Name.Substring(0, index);

            RegistryKey filePathReg = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Siemens\\Automation\\Openness\\14.0\\PublicAPI\\14.0.1.0");

            object oRegKeyValue = filePathReg?.GetValue(name);
            if (oRegKeyValue != null)
            {
                string filePath = oRegKeyValue.ToString();

                string path = filePath;
                string fullPath = Path.GetFullPath(path);
                if (File.Exists(fullPath))
                {
                    return Assembly.LoadFrom(fullPath);
                }
            }
            return null;
        }


        private void updateProjectList()
        {
            frm_ProjectList.Items.Clear();
            frm_ProjectList.BeginUpdate();
            ListViewItem.ListViewSubItem _SubItem;
            for (int i = 0; i < TIAProjects.Count; i++)
            {
                
                ListViewItem _Item = new ListViewItem(TIAProjects[i].Project_Name);
                _Item.ForeColor = Color.Navy;
                _Item.UseItemStyleForSubItems = false;

                _SubItem = new ListViewItem.ListViewSubItem(_Item, TIAProjects[i].Path);
                _SubItem.ForeColor = Color.DarkGray;
                _Item.SubItems.Add(_SubItem);

                _SubItem = new ListViewItem.ListViewSubItem(_Item, TIAProjects[i].Status);
                _SubItem.ForeColor = Color.Navy;
                _Item.SubItems.Add(_SubItem);

                _SubItem = new ListViewItem.ListViewSubItem(_Item, TIAProjects[i].Num_Modules);
                _SubItem.ForeColor = Color.DarkKhaki;
                _Item.SubItems.Add(_SubItem);

                _SubItem = new ListViewItem.ListViewSubItem(_Item, TIAProjects[i].TimeStamp);
                _SubItem.ForeColor = Color.Green;
                _Item.SubItems.Add(_SubItem);

                frm_ProjectList.Items.Add(_Item);

            }

            frm_ProjectList.EndUpdate();
        }

        private void btn_UpdateProjects_Click(object sender, EventArgs e)
        {
            string[] files;
            FolderBrowserDialog fbd = new FolderBrowserDialog();

            DialogResult result = fbd.ShowDialog();

            TIAProjects = new List<TIAProject>();

            if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
            {
                files = Directory.GetFiles(fbd.SelectedPath, "*.ap14", SearchOption.AllDirectories);

                if (files.Length > 0)
                {
                    for (int i = 0; i < files.Length; i++)
                    {
                        TIAProject TIAProj = new TIAProject();

                        TIAProj.Project_Name = Path.GetFileNameWithoutExtension(files[i]);
                        TIAProj.Path = files[i];
                        TIAProj.TimeStamp = string.Empty;
                        TIAProj.TIADevices = new List<TIADevice>();
                        TIAProjects.Add(TIAProj);
                    }
                    updateProjectList();
                    grp_ReadProjects.Enabled = true;
                }
                else
                {
                    MessageBox.Show("No TIA Portal project is found in this directory!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btn_DisposeTIA_Click(object sender, EventArgs e)
        {
            try
            {
                MyTiaPortal.Dispose();
                txt_Status.Text = "TIA Portal disposed";
            }
            catch (Exception exc)
            {
                txt_Status.Text = "Error: TIA Portal dispose error: " + exc.Message;
            }

            btn_StartTIA.Enabled = true;
            btn_DisposeTIA.Enabled = false;
            grp_GetProjects.Enabled = false;
            grp_ReadProjects.Enabled = false;
            grp_tools.Enabled = false;
        }

        private void btn_StartTIA_Click(object sender, EventArgs e)
        {
            txt_Status.Text = "Starting TIA Portal...please select YES in the TIA Openness confirmation window!";
            txt_Status.Refresh();
            FRM_Wait.Visible = true;
            Access.Show();
            try
            {
                if (rdb_WithoutUI.Checked == true)
                {
                    MyTiaPortal = new TiaPortal(TiaPortalMode.WithoutUserInterface);
                    txt_Status.Text = "TIA Portal started without user interface";
                    _tiaProcess = TiaPortal.GetProcesses()[0];
                }
                else
                {
                    MyTiaPortal = new TiaPortal(TiaPortalMode.WithUserInterface);
                    txt_Status.Text = "TIA Portal started with user interface";
                }

                btn_DisposeTIA.Enabled = true;
                btn_StartTIA.Enabled = false;
                grp_tools.Enabled = true;
                grp_GetProjects.Enabled = true;
            }
            catch (Exception exc)
            {
                txt_Status.Text = "Error: TIA Portal refused the connection: " + exc.Message;
                btn_StartTIA.Enabled = true;
                btn_DisposeTIA.Enabled = false;
                grp_GetProjects.Enabled = false;
                grp_ReadProjects.Enabled = false;
                grp_tools.Enabled = false;
            }
            FRM_Wait.Visible = false;
            Access.Hide();

        }

        //Enamerate devices in groups or sub-groups
        private void EnumerateDevicesInGroups(Project project)
        {
            if (project.DeviceGroups != null)
            {
                foreach (DeviceUserGroup deviceUserGroup in project.DeviceGroups)
                {
                    EnumerateDeviceUserGroup(deviceUserGroup);
                }
            }
        }
        private void EnumerateDeviceUserGroup(DeviceUserGroup deviceUserGroup)
        {
            EnumerateDeviceObjects(deviceUserGroup.Devices);
            if (deviceUserGroup.Groups != null)
            {
                foreach (DeviceUserGroup subDeviceUserGroup in deviceUserGroup.Groups)
                {
                    // recursion
                    EnumerateDeviceUserGroup(subDeviceUserGroup);
                }
            }
        }
        private void EnumerateDeviceObjects(DeviceComposition deviceComposition)
        {
            if (deviceComposition != null)
            {
                foreach (Device device in deviceComposition)
                {
                    if ((AktTIADevice != null) && (AktTIADevice.Name != string.Empty))
                    {
                        TIAProjects[AktProjectNumber].TIADevices.Add(AktTIADevice);
                        AktTIADevice = new TIADevice();

                    }
                    if (device.GetAttribute("IsGsd").ToString() == "true")
                    {
                        //GSD Imported Object
                        AktTIADevice.IsGsd = "true";
                        try { AktTIADevice.Name = device.GetAttribute("Name").ToString(); } catch { }
                        try { AktTIADevice.TypeIdentifier = device.GetAttribute("TypeIdentifier").ToString(); } catch { }
                    }
                    else
                    {
                        //Siemens Module
                        AktTIADevice.IsGsd = "false";
                        try { AktTIADevice.Name = device.GetAttribute("Name").ToString(); } catch { }
                        try { AktTIADevice.TypeName = device.GetAttribute("TypeName").ToString(); } catch { }
                        try { AktTIADevice.TypeIdentifier = device.GetAttribute("TypeIdentifier").ToString(); } catch { }
                    }


                    foreach (var deviceItem in device.DeviceItems)
                    {
                        AktTIADeviceItem = new TIADeviceItem();
                        SumModules++;
                        if (AktTIADevice.IsGsd == "true")
                        {
                            //GSD Imported Object
                            try { AktTIADeviceItem.TypeIdentifier = deviceItem.GetAttribute("TypeIdentifier").ToString(); } catch { }
                            try { AktTIADeviceItem.GsdName = deviceItem.GetAttribute("GsdName").ToString(); } catch { }
                            try { AktTIADeviceItem.Name = deviceItem.GetAttribute("Name").ToString(); } catch { }
                        }
                        else
                        {
                            //Siemens Module
                            try { AktTIADeviceItem.TypeName = deviceItem.GetAttribute("TypeName").ToString(); } catch { }
                            try { AktTIADeviceItem.OrderNumber = deviceItem.GetAttribute("OrderNumber").ToString(); } catch { }
                            try { AktTIADeviceItem.Name = deviceItem.GetAttribute("Name").ToString(); } catch { }
                            try { AktTIADeviceItem.FirmwareVersion = deviceItem.GetAttribute("FirmwareVersion").ToString(); } catch { }
                        }
                        AktTIADevice.TIADeviceItems.Add(AktTIADeviceItem);
                    }
                }
            }
        }
        private string ReadTIAProjectModules(bool withAutoUpdate)
        {

            SumModules = 0;
            SumCPUs = 0;

            FRM_Wait.Visible = true;
            string ProjectPath = TIAProjects[AktProjectNumber].Path;
            string retval = string.Empty;

            TIAProjects[AktProjectNumber].Status = "Done";

            frm_Status.Text = "Opening Project..."; frm_Status.Refresh(); Application.DoEvents();

            //Open TIA project
            try
            {
                if (withAutoUpdate)
                {
                    MyProject = MyTiaPortal.Projects.OpenWithUpgrade(new FileInfo(ProjectPath));
                } else
                {
                    MyProject = MyTiaPortal.Projects.Open(new FileInfo(ProjectPath));
                }
                txt_Status.Text = "Project " + ProjectPath + " opened";
                frm_Status.Text = "Project open";frm_Status.Refresh();Application.DoEvents();
            }
            catch (Exception ex)
            {
                txt_Status.Text = "Error while opening project: " + ex.Message;
                TIAProjects[AktProjectNumber].Status = "Can't open TIA Project!";
                MessageBox.Show("Error while opening project: " + ex.Message, "Error in TIA project", MessageBoxButtons.OK, MessageBoxIcon.Error);
                MyProject = null;
            }

            if (MyProject == null)
            {
                TIAProjects[AktProjectNumber].Status = "Can't open TIA Project!";
                txt_Status.Text = "Error while opening project ";
            }

            if (MyProject != null)
            {
                //Enumerate Local Devices
                frm_Status.Text = "Local devices..."; frm_Status.Refresh(); Application.DoEvents();
                if (MyProject.Devices != null)
                {
                    foreach (var device in MyProject.Devices)
                    {

                        if ((AktTIADevice != null) && (AktTIADevice.Name != string.Empty))
                        {
                            TIAProjects[AktProjectNumber].TIADevices.Add(AktTIADevice);
                            AktTIADevice = new TIADevice();
                            
                        }
                        if (device.GetAttribute("IsGsd").ToString() == "true")
                        {
                            //GSD Imported Object
                            AktTIADevice.IsGsd = "true";
                            try { AktTIADevice.Name = device.GetAttribute("Name").ToString(); } catch { }
                            try { AktTIADevice.TypeIdentifier = device.GetAttribute("TypeIdentifier").ToString();} catch { }
                        }
                        else
                        {
                            //Siemens Module
                            AktTIADevice.IsGsd = "false";
                            try { AktTIADevice.Name = device.GetAttribute("Name").ToString(); } catch { }
                            try { AktTIADevice.TypeName = device.GetAttribute("TypeName").ToString(); } catch { }
                            try { AktTIADevice.TypeIdentifier = device.GetAttribute("TypeIdentifier").ToString(); } catch { }
                        }



                        foreach (var deviceItem in device.DeviceItems)
                        {
                            AktTIADeviceItem = new TIADeviceItem();
                            SumModules++;
                            if (AktTIADevice.IsGsd == "true")
                            {
                                //GSD Imported Object
                                try { AktTIADeviceItem.TypeIdentifier = deviceItem.GetAttribute("TypeIdentifier").ToString();}  catch { }
                                try { AktTIADeviceItem.GsdName = deviceItem.GetAttribute("GsdName").ToString();}  catch { }
                                try { AktTIADeviceItem.Name = deviceItem.GetAttribute("Name").ToString();}  catch { }
                            }
                            else
                            {
                                //Siemens Module
                                try { AktTIADeviceItem.TypeName = deviceItem.GetAttribute("TypeName").ToString();  }  catch { }
                                try { AktTIADeviceItem.OrderNumber = deviceItem.GetAttribute("OrderNumber").ToString(); } catch { }
                                try { AktTIADeviceItem.Name = deviceItem.GetAttribute("Name").ToString(); } catch { }
                                try { AktTIADeviceItem.FirmwareVersion = deviceItem.GetAttribute("FirmwareVersion").ToString(); }  catch { }
                            }

                            AktTIADevice.TIADeviceItems.Add(AktTIADeviceItem);
                        }
                    }
                }


                //Enumerate devices of the ungrouped device system group
                DeviceSystemGroup group = MyProject.UngroupedDevicesGroup;
                frm_Status.Text = "Ungrouped devices..."; frm_Status.Refresh(); Application.DoEvents();
                if (group.Devices != null)
                {
                    foreach (var device in group.Devices)
                    {
                        if ((AktTIADevice != null) && (AktTIADevice.Name != string.Empty))
                        {
                            TIAProjects[AktProjectNumber].TIADevices.Add(AktTIADevice);
                            AktTIADevice = new TIADevice();

                        }
                        if (device.GetAttribute("IsGsd").ToString() == "true")
                        {
                            //GSD Imported Object
                            AktTIADevice.IsGsd = "true";
                            try { AktTIADevice.Name = device.GetAttribute("Name").ToString(); } catch { }
                            try { AktTIADevice.TypeIdentifier = device.GetAttribute("TypeIdentifier").ToString(); } catch { }
                        }
                        else
                        {
                            //Siemens Module
                            AktTIADevice.IsGsd = "false";
                            try { AktTIADevice.Name = device.GetAttribute("Name").ToString(); } catch { }
                            try { AktTIADevice.TypeName = device.GetAttribute("TypeName").ToString(); } catch { }
                            try { AktTIADevice.TypeIdentifier = device.GetAttribute("TypeIdentifier").ToString(); } catch { }
                        }



                        foreach (var deviceItem in device.DeviceItems)
                        {
                            AktTIADeviceItem = new TIADeviceItem();
                            SumModules++;
                            if (AktTIADevice.IsGsd == "true")
                            {
                                //GSD Imported Object
                                try { AktTIADeviceItem.TypeIdentifier = deviceItem.GetAttribute("TypeIdentifier").ToString(); } catch { }
                                try { AktTIADeviceItem.GsdName = deviceItem.GetAttribute("GsdName").ToString(); } catch { }
                                try { AktTIADeviceItem.Name = deviceItem.GetAttribute("Name").ToString(); } catch { }
                            }
                            else
                            {
                                //Siemens Module
                                try { AktTIADeviceItem.TypeName = deviceItem.GetAttribute("TypeName").ToString(); } catch { }
                                try { AktTIADeviceItem.OrderNumber = deviceItem.GetAttribute("OrderNumber").ToString(); } catch { }
                                try { AktTIADeviceItem.Name = deviceItem.GetAttribute("Name").ToString(); } catch { }
                                try { AktTIADeviceItem.FirmwareVersion = deviceItem.GetAttribute("FirmwareVersion").ToString(); } catch { }
                            }
                            AktTIADevice.TIADeviceItems.Add(AktTIADeviceItem);
                        }
                    }
                }

                //Enumerate Grouped Devices
                frm_Status.Text = "Grouped devices..."; frm_Status.Refresh(); Application.DoEvents();
                EnumerateDevicesInGroups(MyProject);

                TIAProjects[AktProjectNumber].Num_Modules = SumModules.ToString();
                TIAProjects[AktProjectNumber].Num_CPUs = SumCPUs.ToString();
                TIAProjects[AktProjectNumber].TimeStamp = DateTime.Now.ToString(@"yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);

                //Close Project    
                MyProject.Close();
                txt_Status.Text = "Read done.";
            }
            FRM_Wait.Visible = false;
            frm_Status.Text = ""; frm_Status.Refresh(); Application.DoEvents();
            return retval;
        }

        private void btn_ReadAllProjets_Click(object sender, EventArgs e)
        {
            if (TIAProjects.Count > 0)
            {
                for (int i = 0; i < TIAProjects.Count; i++)
                {
                    AktProjectNumber = i;
                    ReadTIAProjectModules(chk_NeedAutoUpdate.Checked);
                    updateProjectList();


                }
            }
            else
            {
                MessageBox.Show("Use the [Update Project List] button to load TIA projects!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            TIAProjects = TIAProjects;
        }

        private void txt_Status_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn_ExportToText_Click(object sender, EventArgs e)
        {
            if (TIAProjects.Count > 0)
            {
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.Filter = "Text file|*.txt";
                saveFileDialog1.Title = "Save a Text File";
                saveFileDialog1.ShowDialog();

                if (saveFileDialog1.FileName != "")
                {
                    using (StreamWriter sw = File.CreateText(saveFileDialog1.FileName))
                    {
                        for (int i = 0; i < TIAProjects.Count; i++)
                        {
                            sw.WriteLine(new string('*', TIAProjects[i].Path.Length));
                            sw.WriteLine("TIA Project"+ TIAProjects[i].Project_Name);
                            sw.WriteLine(TIAProjects[i].Path);
                            sw.WriteLine(new string('*', TIAProjects[i].Path.Length));
                            
                            for (int dev=0; dev< TIAProjects[i].TIADevices.Count;dev++)
                            {
                                sw.WriteLine("     Device: "+TIAProjects[i].TIADevices[dev].TypeIdentifier);
                                sw.WriteLine("             " + new string('-', TIAProjects[i].TIADevices[dev].TypeIdentifier.Length));
                                sw.WriteLine("             Name:" + TIAProjects[i].TIADevices[dev].Name);
                                sw.WriteLine("             TypeName:" + TIAProjects[i].TIADevices[dev].TypeName);
                                sw.WriteLine("             isGSD:" + TIAProjects[i].TIADevices[dev].IsGsd);

                                for (int devitem = 0; devitem < TIAProjects[i].TIADevices[dev].TIADeviceItems.Count; devitem++)
                                {
                                    if (TIAProjects[i].TIADevices[dev].IsGsd == "true")
                                    {
                                        sw.WriteLine("                 DeviceItem:  Name:" + TIAProjects[i].TIADevices[dev].TIADeviceItems[devitem].Name);
                                        sw.WriteLine("                              TypeIdentifier:" + TIAProjects[i].TIADevices[dev].TIADeviceItems[devitem].TypeIdentifier);
                                        sw.WriteLine("                              GsdName:" + TIAProjects[i].TIADevices[dev].TIADeviceItems[devitem].GsdName);
                                        sw.WriteLine("");
                                    }
                                    else
                                    {
                                        sw.WriteLine("                 DeviceItem:  Name:" + TIAProjects[i].TIADevices[dev].TIADeviceItems[devitem].Name);
                                        sw.WriteLine("                              TypeName:" + TIAProjects[i].TIADevices[dev].TIADeviceItems[devitem].TypeName);
                                        sw.WriteLine("                              FirmwareVersion:" + TIAProjects[i].TIADevices[dev].TIADeviceItems[devitem].FirmwareVersion);
                                        sw.WriteLine("                              OrderNumber:" + TIAProjects[i].TIADevices[dev].TIADeviceItems[devitem].OrderNumber);
                                        sw.WriteLine("");

                                    }

                                }
                            }

                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Use the [Update Project List] button to load TIA projects!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

        }

        private void btn_ExportToTextOnlyOrderNumbers_Click(object sender, EventArgs e)
        {
            if (TIAProjects.Count > 0)
            {
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.Filter = "Text file|*.txt";
                saveFileDialog1.Title = "Save a Text File";
                saveFileDialog1.ShowDialog();

                if (saveFileDialog1.FileName != "")
                {
                    using (StreamWriter sw = File.CreateText(saveFileDialog1.FileName))
                    {
                        for (int i = 0; i < TIAProjects.Count; i++)
                        {
                            sw.WriteLine(new string('*', TIAProjects[i].Path.Length));
                            sw.WriteLine("TIA Project" + TIAProjects[i].Project_Name);
                            sw.WriteLine(TIAProjects[i].Path);
                            sw.WriteLine(new string('*', TIAProjects[i].Path.Length));

                            for (int dev = 0; dev < TIAProjects[i].TIADevices.Count; dev++)
                            {

                                for (int devitem = 0; devitem < TIAProjects[i].TIADevices[dev].TIADeviceItems.Count; devitem++)
                                {
                                    if (TIAProjects[i].TIADevices[dev].TIADeviceItems[devitem].OrderNumber != string.Empty)
                                    {
                                        sw.WriteLine("        OrderNumber:" + TIAProjects[i].TIADevices[dev].TIADeviceItems[devitem].OrderNumber);
                                        sw.WriteLine("");
                                    }

                                }
                            }

                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Use the [Update Project List] button to load TIA projects!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

        }

        private void grp_TIA_Enter(object sender, EventArgs e)
        {

        }
    }
}
