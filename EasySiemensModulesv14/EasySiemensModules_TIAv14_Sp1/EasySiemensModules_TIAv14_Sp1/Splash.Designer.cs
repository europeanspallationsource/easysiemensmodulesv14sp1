﻿namespace EasySiemensModules_TIAv14_Sp1
{
    partial class SplashForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SplashPicture = new System.Windows.Forms.PictureBox();
            this.lbl_CopyRight = new System.Windows.Forms.Label();
            this.lbl_Title = new System.Windows.Forms.Label();
            this.lbl_Version = new System.Windows.Forms.Label();
            this.SplashTimer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.SplashPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // SplashPicture
            // 
            this.SplashPicture.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplashPicture.Image = global::EasySiemensModules_TIAv14_Sp1.Properties.Resources.Splash;
            this.SplashPicture.Location = new System.Drawing.Point(0, 0);
            this.SplashPicture.Name = "SplashPicture";
            this.SplashPicture.Size = new System.Drawing.Size(756, 426);
            this.SplashPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.SplashPicture.TabIndex = 0;
            this.SplashPicture.TabStop = false;
            // 
            // lbl_CopyRight
            // 
            this.lbl_CopyRight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbl_CopyRight.AutoSize = true;
            this.lbl_CopyRight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl_CopyRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl_CopyRight.ForeColor = System.Drawing.Color.Aqua;
            this.lbl_CopyRight.Location = new System.Drawing.Point(12, 397);
            this.lbl_CopyRight.Name = "lbl_CopyRight";
            this.lbl_CopyRight.Size = new System.Drawing.Size(502, 20);
            this.lbl_CopyRight.TabIndex = 1;
            this.lbl_CopyRight.Text = "Copyright 2019 by Miklos Boros, source @ bitBucket SpallationSource";
            // 
            // lbl_Title
            // 
            this.lbl_Title.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbl_Title.AutoSize = true;
            this.lbl_Title.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl_Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl_Title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lbl_Title.Location = new System.Drawing.Point(359, 5);
            this.lbl_Title.Name = "lbl_Title";
            this.lbl_Title.Size = new System.Drawing.Size(393, 46);
            this.lbl_Title.TabIndex = 2;
            this.lbl_Title.Text = "EasySiemensModuls";
            // 
            // lbl_Version
            // 
            this.lbl_Version.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbl_Version.AutoSize = true;
            this.lbl_Version.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl_Version.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl_Version.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lbl_Version.Location = new System.Drawing.Point(624, 51);
            this.lbl_Version.Name = "lbl_Version";
            this.lbl_Version.Size = new System.Drawing.Size(120, 20);
            this.lbl_Version.TabIndex = 3;
            this.lbl_Version.Text = "for TIA v14 SP1";
            // 
            // SplashTimer
            // 
            this.SplashTimer.Enabled = true;
            this.SplashTimer.Interval = 2500;
            this.SplashTimer.Tick += new System.EventHandler(this.SplashTimer_Tick);
            // 
            // SplashForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(756, 426);
            this.Controls.Add(this.lbl_Version);
            this.Controls.Add(this.lbl_Title);
            this.Controls.Add(this.lbl_CopyRight);
            this.Controls.Add(this.SplashPicture);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SplashForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "v";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.SplashPicture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox SplashPicture;
        private System.Windows.Forms.Label lbl_CopyRight;
        private System.Windows.Forms.Label lbl_Title;
        private System.Windows.Forms.Label lbl_Version;
        private System.Windows.Forms.Timer SplashTimer;
    }
}