﻿namespace EasySiemensModules_TIAv14_Sp1
{
    partial class TIAaccess
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FRM_Wait = new System.Windows.Forms.PictureBox();
            this.lbl_Step1 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.FRM_Wait)).BeginInit();
            this.SuspendLayout();
            // 
            // FRM_Wait
            // 
            this.FRM_Wait.Image = global::EasySiemensModules_TIAv14_Sp1.Properties.Resources.TIAaccess;
            this.FRM_Wait.Location = new System.Drawing.Point(44, 95);
            this.FRM_Wait.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.FRM_Wait.Name = "FRM_Wait";
            this.FRM_Wait.Size = new System.Drawing.Size(455, 309);
            this.FRM_Wait.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.FRM_Wait.TabIndex = 38;
            this.FRM_Wait.TabStop = false;
            // 
            // lbl_Step1
            // 
            this.lbl_Step1.AutoSize = true;
            this.lbl_Step1.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Step1.Font = new System.Drawing.Font("Arial", 20F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl_Step1.ForeColor = System.Drawing.Color.Navy;
            this.lbl_Step1.Location = new System.Drawing.Point(12, 21);
            this.lbl_Step1.Name = "lbl_Step1";
            this.lbl_Step1.Size = new System.Drawing.Size(192, 32);
            this.lbl_Step1.TabIndex = 39;
            this.lbl_Step1.Text = "Allow access!";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(12, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(510, 19);
            this.label1.TabIndex = 40;
            this.label1.Text = "Check TIA Portal Openness access window and select YES to proceed!";
            // 
            // TIAaccess
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(554, 439);
            this.ControlBox = false;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbl_Step1);
            this.Controls.Add(this.FRM_Wait);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "TIAaccess";
            this.Text = "TIA Portal access";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.FRM_Wait)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox FRM_Wait;
        private System.Windows.Forms.Label lbl_Step1;
        private System.Windows.Forms.Label label1;
    }
}